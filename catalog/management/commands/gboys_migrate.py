# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand
from catalog.models import Category, Tag, Game
from django.db.models import F


class Command(BaseCommand):

    def handle(self, *args, **options):

        print(Category.objects.all().exclude(description_game_boys='').update(description_ru=F('description_game_boys')))
        print(Tag.objects.all().exclude(description_game_boys='').update(description_ru=F('description_game_boys')))
        print(Game.objects.all().exclude(description_game_boys='').update(description_ru=F('description_game_boys')))
        print(Game.objects.all().exclude(control_game_boys='').update(control_ru=F('control_game_boys')))
        print(Game.objects.all().exclude(video_game_boys='').update(video_ru=F('video_game_boys')))
