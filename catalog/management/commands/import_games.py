# -*- coding: utf-8 -*-
import tablib
import xlrd
from tqdm import tqdm

from django.core.management.base import BaseCommand
from import_export import resources

from catalog.models import Game


class Command(BaseCommand):

    def handle(self, *args, **options):

        workbook = xlrd.open_workbook('games.xlsx')
        sheet = workbook.sheet_by_index(0)

        for rownum in tqdm(range(sheet.nrows)[1:]):
            row = sheet.row_values(rownum)
            try:
                obj = Game.objects.get(pk=row[0])
                obj.name_ar = row[2]
                obj.save()
            except:
                pass
