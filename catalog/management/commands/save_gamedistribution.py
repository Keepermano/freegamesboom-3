# -*- coding: utf-8 -*-
import json
from io import BytesIO

import requests
from django.core import files
from django.core.management.base import BaseCommand
from django.db import IntegrityError
from tqdm import tqdm

from catalog.models import Game, Category, Tag, GamesTranslation


class Command(BaseCommand):

    def handle(self, *args, **options):
        cats = []
        new = 0
        update = 0
        mapping = {
            'Girls': Category.objects.get(slug='for-girls'),
            '.IO': Category.objects.get(slug='io-games'),
            'Soccer': Tag.objects.get(slug='soccer-games'),
            'Stickman': Tag.objects.get(slug='stickman-games'),
            'Social': Category.objects.get(slug='adventure-games'),
            'Clicker': Tag.objects.get(slug='clicker'),
            'Multiplayer': Category.objects.get(slug='multiplayer-games'),
            'Farming': Tag.objects.get(slug='farming'),
            'Boys': Tag.objects.get(slug='boys'),
            'Hypercasual': Category.objects.get(slug='action-games'),
            'Cooking': Tag.objects.get(slug='cooking-games'),
            'Arcade': Category.objects.get(slug='arcade-games'),
            '2 Player': Tag.objects.get(slug='2-player-games'),
            'Puzzle': Category.objects.get(slug='puzzle-games'),
            'Racing': Category.objects.get(slug='racing-games'),
            'Adventure': Category.objects.get(slug='adventure-games'),
            'Bejeweled': Category.objects.get(slug='puzzle-games'),
            'Baby': Tag.objects.get(slug='children-games'),
            'Sports': Category.objects.get(slug='sports-games'),
            '3D': Category.objects.get(slug='3d-games'),
            'Shooting': Category.objects.get(slug='shooting-games'),
            'Action': Category.objects.get(slug='action-games')
        }
        with open('gamedistribution.com.json', 'r') as f:
            data = json.loads(f.read())
            for item in tqdm(data):
                try:
                    Game.objects.get(name=item['title'])
                    # game.service = 2
                    # game.link = 'https://{type}.gamedistribution.com/{md5}/'.format(
                    #     type=item['type'],
                    #     md5=item['md5'],
                    #     height=item['height'] if item['height'] else 'null',
                    #     width=item['width'] if item['width'] else 'null',
                    # )
                    # game.mobile = True if item['mobile'] else False
                    # game.save()
                    #
                    # # save image
                    # if item['image'] and item['image'] != '':
                    #     resp = requests.get(item['image'])
                    #     if resp.status_code != requests.codes.ok:
                    #         print(resp.status_code)
                    #     fp = BytesIO()
                    #     fp.write(resp.content)
                    #     file_name = item['image'].split("/")[-1]
                    #     game.image.save(file_name, files.File(fp))
                    #
                    # update += 1
                except Game.DoesNotExist:

                    if item['categories']:
                        game = Game()
                        try:
                            mapping_obj = mapping[item['categories'][0]]
                        except IndexError:
                            continue
                        if isinstance(mapping_obj, Category):
                            game.category = mapping_obj
                        game.name = item['title']
                        game.service = 2
                        game.link = 'https://{type}.gamedistribution.com/{md5}/'.format(
                            type=item['type'],
                            md5=item['md5'],
                            height=item['height'] if item['height'] else 'null',
                            width=item['width'] if item['width'] else 'null',
                        )
                        game.mobile = True if item['mobile'] else False
                        game.save()

                        try:
                            GamesTranslation.objects.create(
                                text_block_code='description',
                                language_code='en',
                                text=item['description'],
                                game_id=game.id
                            )
                        except IntegrityError:
                            pass

                        # save image
                        if item['image'] and item['image'] != '':
                            resp = requests.get(item['image'])
                            if resp.status_code != requests.codes.ok:
                                print(resp.status_code)
                            fp = BytesIO()
                            fp.write(resp.content)
                            file_name = item['image'].split("/")[-1]
                            game.image.save(file_name, files.File(fp))

                        # save tag if need
                        if isinstance(mapping_obj, Tag):
                            game.tags.add(mapping_obj)

                    new += 1
        print(new)

