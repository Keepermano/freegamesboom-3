# -*- coding: utf-8 -*-
from django.conf import settings as django_settings
from .models import Settings, MenuHead, MenuFooter, Counter, Banner, MenuButton
from catalog.models import Block, TranslateLanguage


def settings(request):
    site_setting = Settings.load()
    languages = TranslateLanguage.public.all()
    return {
        'scheme': 'http' if django_settings.DEBUG else 'https',
        'LANGUAGES_ISO_CONVERT': django_settings.LANGUAGES_ISO_CONVERT,
        'settings': site_setting,
        'banner_top': Banner.published.filter(type=1).first(),
        'banner_right': Banner.published.filter(type=2).first(),
        'total_items': site_setting.count_items,
        'footer_social': site_setting.footer_social,
        'menu_header': MenuHead.objects.all(),
        'menu_footer': MenuFooter.objects.all(),
        'menu_button': MenuButton.objects.all(),
        'counters_top': Counter.objects.filter(active=True, type=1),
        'counters_bottom': Counter.objects.filter(active=True, type=2),
        'block_vertical': Block.objects.filter(type=2).prefetch_related('items__tag'),
        'block_horizontal': Block.objects.filter(type=1).prefetch_related('items__tag'),
        'languages_processed': languages,
        'languages_processed_dict': {x.slug: x.country_code for x in languages},
        'languages_processed_locales': {x.slug: x.locale for x in languages},
    }
