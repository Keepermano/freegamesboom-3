# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-05-14 17:54+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n==1) ? 0 : (n==2) ? 1 : (n != 8 && n != "
"11) ? 2 : 3;\n"

#: catalog/fields.py:23
msgid "Filetype not supported."
msgstr "Ni chefnogir Filetype."

#: catalog/templates/catalog/base.html:71
msgid "free games BOOM"
msgstr "gemau am ddim BOOM"

#: catalog/templates/catalog/base.html:78
#: catalog/templates/catalog/base.html:133
msgid "Find more 100000 games"
msgstr "Dewch o hyd i fwy o 100000 o gemau"

#: catalog/templates/catalog/base.html:79
#: catalog/templates/catalog/base.html:134
#: catalog/templates/catalog/search.html:7
msgid "Search"
msgstr "Chwilio"

#: catalog/templates/catalog/base.html:97
#: catalog/templates/catalog/inclusions/sort_block.html:6
msgid "New"
msgstr "Newydd"

#: catalog/templates/catalog/base.html:103
msgid "Popular"
msgstr "Poblogaidd"

#: catalog/templates/catalog/base.html:114
msgid "Favourite <br>games"
msgstr "Hoff <br>gemau"

#: catalog/templates/catalog/base.html:125
msgid "Last <br>played"
msgstr "Chwaraeodd <br>diwethaf"

#: catalog/templates/catalog/category.html:95
msgid "Information"
msgstr "Gwybodaeth"

#: catalog/templates/catalog/game.html:90
msgid "Category"
msgstr "Categori"

#: catalog/templates/catalog/game.html:105
msgid "Game"
msgstr "Gêm"

#: catalog/templates/catalog/game.html:131
#: catalog/templates/catalog/game.html:133
msgid "PLAY"
msgstr "CHWARAE"

#: catalog/templates/catalog/game.html:142
msgid "This game is not available on mobile."
msgstr "Nid yw'r gêm hon ar gael ar ffôn symudol."

#: catalog/templates/catalog/game.html:174
msgid "Controls"
msgstr "Rheolaethau"

#: catalog/templates/catalog/game.html:181
msgid "Video"
msgstr "Fideo"

#: catalog/templates/catalog/inclusions/_include_game_list.html:19
msgid "Play!"
msgstr "Chwarae!"

#: catalog/templates/catalog/inclusions/category_menu.html:9
msgid "TOP CATEGORIES"
msgstr "CATEGORIESAU TOP"

#: catalog/templates/catalog/inclusions/game_block.html:5
msgid "votes"
msgstr "pleidleisiau"

#: catalog/templates/catalog/inclusions/game_block.html:11
msgid "Doesn’t  <br>work?"
msgstr "Onid yw <br> yn gweithio?"

#: catalog/templates/catalog/inclusions/game_block.html:17 catalog/views.py:226
msgid "Added"
msgstr ""

#: catalog/templates/catalog/inclusions/game_block.html:17
msgid "Add to <br>favorites"
msgstr "Wedi adio"

#: catalog/templates/catalog/inclusions/game_block.html:35
msgid "Share"
msgstr "Rhannu"

#: catalog/templates/catalog/inclusions/game_block.html:41
msgid "Fullscreen"
msgstr "Sgrin llawn"

#: catalog/templates/catalog/inclusions/popup-games.html:7
msgid "Clear list"
msgstr "Rhestr glir"

#: catalog/templates/catalog/inclusions/sort_block.html:3
msgid "Sort by"
msgstr "Trefnu yn ôl"

#: catalog/templates/catalog/inclusions/sort_block.html:3
#: catalog/templates/catalog/inclusions/sort_block.html:5
msgid "Most Popular"
msgstr "Mwyaf poblogaidd"

#: catalog/templates/catalog/inclusions/sort_block.html:7
msgid "A-Z"
msgstr "A-Y"

#: catalog/templates/catalog/tags.html:6
msgid "Tags"
msgstr "Tagiau"

#: catalog/templates/catalog/tags.html:11
msgid "Popular tags"
msgstr "Tagiau poblogaidd"

#: catalog/templatetags/catalog_tags.py:45
msgid "MOST POPULAR GAMES"
msgstr "GEMAU POPULAR MWYAF"

#: catalog/templatetags/catalog_tags.py:64
msgid "POPULAR TAGS"
msgstr "TAGIAU POBLOGAIDD"

#: catalog/templatetags/catalog_tags.py:87
msgid "MOST POPULAR"
msgstr "MWYAF POBLOGAIDD"

#: catalog/templatetags/catalog_tags.py:156
msgid "FGM RECOMMENDED!"
msgstr "ARGYMHELLIR FGM!"

#: catalog/templatetags/catalog_tags.py:160
#, python-format
msgid "MORE %(category)s"
msgstr "MWY %(category)s"

#: catalog/templatetags/catalog_tags.py:187
msgid "LAST PLAYED"
msgstr "CHWARAE DIWETHAF"

#: catalog/templatetags/catalog_tags.py:282 catalog/views.py:71
msgid "New games"
msgstr "Gemau newydd"

#: catalog/views.py:70
msgid "New games - Free Games Boom.com"
msgstr "Gemau newydd - Free Games Boom.com"

#: catalog/views.py:74
msgid "Popular games - Free Games Boom.com"
msgstr "Gemau poblogaidd - Free Games Boom.com"

#: catalog/views.py:75
msgid "Popular games"
msgstr "Gemau poblogaidd"

#: catalog/views.py:99
#, fuzzy, python-format
#| msgid "Popular games"
msgid "Similar %s games"
msgstr "Gemau poblogaidd"

#: catalog/views.py:191
msgid "Favourite games"
msgstr "Hoff gemau"

#: catalog/views.py:194
msgid "Last Played"
msgstr "Chwaraewyd Diwethaf"

#: flatpages/apps.py:7
msgid "Flat Pages"
msgstr "Tudalennau Fflat"

#: flatpages/models.py:12
msgid "Title"
msgstr "Teitl"

#: flatpages/models.py:14
msgid "Text"
msgstr "Testun"

#: flatpages/models.py:15
msgid "Active"
msgstr "Egnïol"

#: flatpages/models.py:16
msgid "Modified"
msgstr "Wedi'i addasu"

#: flatpages/models.py:19
msgid "Static pages"
msgstr "Tudalennau statig"

#: flatpages/models.py:20
msgid "Static page"
msgstr "Tudalen statig"

#: freegamesboom/settings.py:155
msgid "Afrikaans"
msgstr "Affricaneg"

#: freegamesboom/settings.py:156
msgid "Arabic"
msgstr "Arabeg"

#: freegamesboom/settings.py:157
msgid "Azerbaijani"
msgstr "Aserbaijani"

#: freegamesboom/settings.py:158
msgid "Bulgarian"
msgstr "Bwlgaria"

#: freegamesboom/settings.py:159
msgid "Belarusian"
msgstr "Belarwseg"

#: freegamesboom/settings.py:160
msgid "Bengali"
msgstr "Bengali"

#: freegamesboom/settings.py:161
msgid "Bosnian"
msgstr "Bosnia"

#: freegamesboom/settings.py:162
msgid "Catalan"
msgstr "Catalaneg"

#: freegamesboom/settings.py:163
msgid "Czech"
msgstr "Tsiec"

#: freegamesboom/settings.py:164
msgid "Welsh"
msgstr "Cymraeg"

#: freegamesboom/settings.py:165
msgid "Danish"
msgstr "Daneg"

#: freegamesboom/settings.py:166
msgid "German"
msgstr "Almaeneg"

#: freegamesboom/settings.py:167
msgid "Greek"
msgstr "Groeg"

#: freegamesboom/settings.py:168
msgid "English"
msgstr "Saesneg"

#: freegamesboom/settings.py:169
msgid "Esperanto"
msgstr "Esperanto"

#: freegamesboom/settings.py:170
msgid "Spanish"
msgstr "Sbaeneg"

#: freegamesboom/settings.py:171
msgid "Estonian"
msgstr "Estoneg"

#: freegamesboom/settings.py:172
msgid "Finnish"
msgstr "Ffinneg"

#: freegamesboom/settings.py:173
msgid "French"
msgstr "Ffrangeg"

#: freegamesboom/settings.py:174
msgid "Irish"
msgstr "Gwyddeleg"

#: freegamesboom/settings.py:175
msgid "Galician"
msgstr "Galisia"

#: freegamesboom/settings.py:176
msgid "Hebrew"
msgstr "Hebraeg"

#: freegamesboom/settings.py:177
msgid "Hindi"
msgstr "Hindi"

#: freegamesboom/settings.py:178
msgid "Croatian"
msgstr "Croateg"

#: freegamesboom/settings.py:179
msgid "Hungarian"
msgstr "Hwngari"

#: freegamesboom/settings.py:180
msgid "Armenian"
msgstr "Armeneg"

#: freegamesboom/settings.py:182
msgid "Italian"
msgstr "Eidaleg"

#: freegamesboom/settings.py:183
msgid "Japanese"
msgstr "Japaneaidd"

#: freegamesboom/settings.py:184
msgid "Georgian"
msgstr "Sioraidd"

#: freegamesboom/settings.py:186
msgid "Kazakh"
msgstr "Kazakh"

#: freegamesboom/settings.py:187
msgid "Khmer"
msgstr "Khmer"

#: freegamesboom/settings.py:188
msgid "Kannada"
msgstr "Kannada"

#: freegamesboom/settings.py:189
msgid "Korean"
msgstr "Corea"

#: freegamesboom/settings.py:190
msgid "Luxembourgish"
msgstr "Lwcsembwrg"

#: freegamesboom/settings.py:191
msgid "Lithuanian"
msgstr "Lithwaneg"

#: freegamesboom/settings.py:192
msgid "Latvian"
msgstr "Latfia"

#: freegamesboom/settings.py:193
msgid "Macedonian"
msgstr "Macedoneg"

#: freegamesboom/settings.py:194
msgid "Malayalam"
msgstr "Malayalam"

#: freegamesboom/settings.py:195
msgid "Mongolian"
msgstr "Mongoleg"

#: freegamesboom/settings.py:196
msgid "Marathi"
msgstr "Marathi"

#: freegamesboom/settings.py:197
msgid "Burmese"
msgstr "Byrmaneg"

#: freegamesboom/settings.py:198
msgid "Nepali"
msgstr "Nepali"

#: freegamesboom/settings.py:199
msgid "Dutch"
msgstr "Iseldireg"

#: freegamesboom/settings.py:200
msgid "Ossetic"
msgstr "Ossetig"

#: freegamesboom/settings.py:201
msgid "Punjabi"
msgstr "Pwnjabi"

#: freegamesboom/settings.py:202
msgid "Polish"
msgstr "Pwyleg"

#: freegamesboom/settings.py:203
msgid "Portuguese"
msgstr "Portiwgaleg"

#: freegamesboom/settings.py:204
msgid "Romanian"
msgstr "Rwmaneg"

#: freegamesboom/settings.py:205
msgid "Russian"
msgstr "Rwseg"

#: freegamesboom/settings.py:206
msgid "Slovak"
msgstr "Slofacia"

#: freegamesboom/settings.py:207
msgid "Slovenian"
msgstr "Slofenia"

#: freegamesboom/settings.py:208
msgid "Albanian"
msgstr "Albaneg"

#: freegamesboom/settings.py:209
msgid "Serbian"
msgstr "Serbeg"

#: freegamesboom/settings.py:210
msgid "Swedish"
msgstr "Sweden"

#: freegamesboom/settings.py:211
msgid "Swahili"
msgstr "Swahili"

#: freegamesboom/settings.py:212
msgid "Tamil"
msgstr "Tamil"

#: freegamesboom/settings.py:213
msgid "Telugu"
msgstr "Telugu"

#: freegamesboom/settings.py:214
msgid "Thai"
msgstr "Thai"

#: freegamesboom/settings.py:215
msgid "Turkish"
msgstr "Twrceg"

#: freegamesboom/settings.py:216
msgid "Tatar"
msgstr "Tatar"

#: freegamesboom/settings.py:218
msgid "Ukrainian"
msgstr "Wcreineg"

#: freegamesboom/settings.py:219
msgid "Urdu"
msgstr "Wrdw"

#: freegamesboom/settings.py:220
msgid "Vietnamese"
msgstr "Fietnam"

#: freegamesboom/settings.py:221
msgid "Simplified Chinese"
msgstr "Tsieineaidd symlach"

#: templates/404.html:18
msgid "404 Error"
msgstr "404 Gwall"

#: templates/404.html:19
msgid "Oops. This page does not exist."
msgstr "Wps. Nid yw'r dudalen hon yn bodoli."

#: templates/el_pagination/show_more.html:5
msgid "more"
msgstr "mwy"

#: templates/el_pagination/show_pages.html:6
msgid "All"
msgstr "GYD"

#~ msgid "Indonesian"
#~ msgstr "Indonesia"

#~ msgid "Kabyle"
#~ msgstr "Kabyle"

#~ msgid "Udmurt"
#~ msgstr "Udmurt"
