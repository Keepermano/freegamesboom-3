from django.contrib import admin
from django.contrib import messages

from .models import PushUser, PushNotification


@admin.register(PushUser)
class PushUserAdmin(admin.ModelAdmin):
    list_display = ('id', 'created', 'ip', 'active')
    readonly_fields = ('token', 'ip', 'headers',)
    list_filter = ('active', )


@admin.register(PushNotification)
class PushNotificationAdmin(admin.ModelAdmin):
    list_display = ('title', 'body', 'created', 'processed')
    readonly_fields = ('report', 'processed')

    def has_change_permission(self, request, obj=None):
        return False

    # def save_model(self, request, obj, form, change):
    #     super().save_model(request, obj, form, change)
    #     result = obj.send_message()
    #     obj.report = str(result)
    #     obj.save()
    #     messages.add_message(request, messages.INFO, result)
