const firebaseConfig = {
    apiKey: "AIzaSyDFcQFfaIf4tkEHyEadRmjxvZVYsT_LDT0",
    authDomain: "push-254808.firebaseapp.com",
    databaseURL: "https://push-254808.firebaseio.com",
    projectId: "push-254808",
    storageBucket: "",
    messagingSenderId: "795521926689",
    appId: "1:795521926689:web:8a36fe9be638296b654560"
};
firebase.initializeApp(firebaseConfig);

if ('Notification' in window) {
    var messaging = firebase.messaging();
    subscribeNotification();
}

// Request notification
function subscribeNotification() {
    messaging.requestPermission().then(function () {
        return messaging.getToken()
    }).then(function (token) {
        if (token) {
            console.log(token);
            sendTokenToServer(token)
        } else {
            console.log("Не удалось получить токен.");
            setTokenSentToServer(false);
        }
    }).catch(function (err) {
        console.log("Не удалось получить разрешение на показ уведомлений.", err);
        setTokenSentToServer(false);
    });
}
function sendTokenToServer(token) {
    if (!isTokenSentToServer(token)) {
        $.post('https://freegamesboom.com/push/add_token/', {token: token});
        setTokenSentToServer(token);
    } else {
        console.log('Токен уже был отправлен на сервер.');
    }
}
// используем localStorage для отметки того, что пользователь уже подписался на уведомления
function isTokenSentToServer(token) {
    return window.localStorage.getItem('sentFirebaseMessagingToken') === token;
}
function setTokenSentToServer(token) {
    window.localStorage.setItem(
        'sentFirebaseMessagingToken',
        token ? token : ''
    );
}
